# bitcoin-price-index :chart_with_upwards_trend: :chart_with_downwards_trend:

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/bitcoin-price-index/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/bitcoin-price-index?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/bitcoin-price-index?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/bitcoin-price-index?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/bitcoin-price-index)](https://replit.com/@KennyOliver/bitcoin-price-index)

**Bitcoin price index in USD using an API once per minute**

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/bitcoin-price-index)](https://kennyoliver.github.io/bitcoin-price-index)

---
Kenny Oliver ©2021
